#!/usr/bin/env bash
    
function generateCode(){
    local domainId=$1
    local numberofEntity=$2
    local entityOrDatatype=$3

    local domainModelPath=${domainId}-domain/src/model/
    if [[ ! -d "$domainModelPath" ]]; then
        mkdir -p "$domainModelPath"
    fi
    uiModelPath=${domainId}-web/model/
    if [[ ! -d "${uiModelPath}" ]]; then
        mkdir -p "$uiModelPath"
    fi
    
    echo "number of entities to create: $numberofEntity"
    
    for i in $(seq 1 $numberofEntity); do
    echo "package com.example.foo.domain.performance {
    import com.uniquesoft.gwt.shared.common.pager.PagedResult
    entity Foo${i} {
        name : String
    }" >> ${domainModelPath}/gwtperformance${i}.edl

    if [[ $entityOrDatatype = "datatype" ]];then
    echo "
    datatype Foo${i}DTO{
       required name : String
    }" >> ${domainModelPath}/gwtperformance${i}.edl
    fi
    
    echo "
    criteria Foo${i}Criteria for Foo${i} {
        name : String
        condition self.name = :name
    }
    service Foo${i}Service {
        op createFoo${i}():Foo${i}{
            create Foo${i}()
        }">> ${domainModelPath}/gwtperformance${i}.edl

    if [[ $entityOrDatatype = "datatype" ]];then
    echo "
        op lookupFoo(criteriaName: String, page: Integer, pageSize:Integer):PagedResult<Foo${i}DTO>{
            val query := findby Foo${i}Criteria=>[
                name := criteriaName
                it.^start := page - 1
                it.^limit := pageSize
            ]
            val content := query.list.^map[ c|
                new Foo${i}DTO()=>[it.name := c.name]
            ].toList

            val all := query.^count()

            new PagedResult<Foo${i}DTO>(page, all, content)
        }" >> ${domainModelPath}/gwtperformance${i}.edl
    fi
echo "
    }
}" >> ${domainModelPath}/gwtperformance${i}.edl
    
    echo "
    package com.example.foo.web feature performance {
    
    import com.example.foo.domain.performance.Foo${i}
    import com.example.foo.domain.performance.Foo${i}Criteria
    import edu.fudan.langlab.uidl.annotations.GenerateFieldAccessor
    import com.example.foo.domain.performance.Foo${i}Service" >> ${uiModelPath}/gwtperformance${i}.uidl

    if [[ $entityOrDatatype = "datatype" ]];then
    echo "
    import com.uniquesoft.gwt.shared.common.pager.PagedResult
    import com.example.foo.domain.performance.Foo${i}DTO" >> ${uiModelPath}/gwtperformance${i}.uidl
    fi
    
    echo "
    @GenerateFieldAccessor" >> ${uiModelPath}/gwtperformance${i}.uidl

    if [[ $entityOrDatatype = "entity" ]];then
    echo "
        ui-entity UIFoo${i} for Foo${i} {
            name : String \"名字\"
            map Foo${i}Mapper {
                direct { name }
            }
        }" >> ${uiModelPath}/gwtperformance${i}.uidl
    fi

    if [[ $entityOrDatatype = "datatype" ]];then
    echo "
        ui-datatype UIFoo${i}DTO for Foo${i}DTO {
        	name : String \"名字\"
        	map Foo${i}DTOMapper {
        		direct { name }
        	}
        }" >> ${uiModelPath}/gwtperformance${i}.uidl
    fi

    #if [[ $entityOrDatatype = "entity" ]];then
    echo "
        ui-criteria UIFoo${i}Criteria for Foo${i}Criteria { 
            map Foo${i}CriteriaMapper {
                direct { name }
            }" >> ${uiModelPath}/gwtperformance${i}.uidl
    #fi

    #if [[ $entityOrDatatype = "datatype" ]];then
    #echo "
    #    ui-criteria UIFoo${i}Criteria { " >> ${uiModelPath}/gwtperformance${i}.uidl
    #fi

    echo "
            name : String
            component Foo${i}CriteriaForm as GXTCriteriaForm {
                binding { name }
                op createWidget(): Widget {
                    TABLE=>[
                        tr=>[
                            td=>[it += name.asWidget]
                            td=>[it += searchButton]
                        ]
                    ]
                }
            }
        }
    
        primary-presenter UIFoo${i}Presenter {
            refresh on Foo${i}Changed">> ${uiModelPath}/gwtperformance${i}.uidl

       if [[ $entityOrDatatype = "entity" ]];then
       echo "
            type UIFoo${i}[*] { " >> ${uiModelPath}/gwtperformance${i}.uidl
       fi
       if [[ $entityOrDatatype = "datatype" ]];then
       echo "
            type UIFoo${i}DTO[*] { " >> ${uiModelPath}/gwtperformance${i}.uidl
       fi


       echo "
                component as GXTGrid{
                    binding { name }
                }
            }
            context c : UIFoo${i}Criteria {
                ui-adapt {">> ${uiModelPath}/gwtperformance${i}.uidl
       if [[ $entityOrDatatype = "entity" ]];then
       echo "
                    val c = context.createPagedCriteria(Foo${i}Criteria, @Inject(Foo${i}CriteriaMapper), entities)
                    c.listAndTransform[@Inject(Foo${i}Mapper).apply(it)]
                    ">>${uiModelPath}/gwtperformance${i}.uidl
       fi

       if [[ $entityOrDatatype = "datatype" ]];then
       echo "
                    val result = @Inject(Foo${i}Service).lookupFoo(context.name, context.page, context.pageSize)
                	return new PagedResult(result.page, result.totalRecords,
                		result.^map[@Inject(Foo${i}DTOMapper).transform(it)].toList
                	);
                    ">>${uiModelPath}/gwtperformance${i}.uidl
       fi

       echo "

                }
            }
        }
    
        action CreateFoo${i}Action '新建'('新建') {" >>${uiModelPath}/gwtperformance${i}.uidl
        if [[ $entityOrDatatype = "entity" ]]; then
        echo "
            type UIFoo${i} {" >> ${uiModelPath}/gwtperformance${i}.uidl
        fi
        if [[ $entityOrDatatype = "datatype" ]]; then
        echo "
            type UIFoo${i}DTO {" >> ${uiModelPath}/gwtperformance${i}.uidl
        fi
        echo "
                component as GXTForm {
                    binding { name }
                    op createWidget(): Widget {
                        widgets.VLayout=>[
                            addFill(name.asWidget)
                        ]
                    }
                }
            }
            submit {
                @Inject(Foo${i}Service).createFoo${i}=>[">>${uiModelPath}/gwtperformance${i}.uidl
       if [[ $entityOrDatatype = "entity" ]];then
       echo "
                    @Inject(Foo${i}Mapper).transform(uiValue, it)">>${uiModelPath}/gwtperformance${i}.uidl
       fi
       if [[ $entityOrDatatype = "datatype" ]];then
       echo "
                    it.name = uiValue.name">>${uiModelPath}/gwtperformance${i}.uidl
       fi

       echo "
                ]
            }
            success {
                fire eventbus.Foo${i}Changed
            }
        }
    
        composite-presenter Foo${i}Composite {
            parts {
                criteriaForm : Foo${i}CriteriaForm { provide-context { criteriaContext : UIFoo${i}Criteria }}
                list : UIFoo${i}Presenter require-context c {
                    connect-to criteriaForm.criteriaContext
                }
                createActoin : CreateFoo${i}Action
            }
            layout {
                widgets.MainFramedPanel(\"\")=>[
                    widget=widgets.CardPanel=>[
                        widget = (criteriaForm.bind(list))
                        addCommand(createActoin.asCommand)
                    ]
                ]
    
            }
        }
        ui-command ShowFoo${i}CompositeCommand 'Foo${i}2'{
            factory p : Foo${i}Composite
            execute {
                p.get.setup[
                    fire eventbus.ShowActivePresenter(it)
                ]
            }
        }
    
        signal Foo${i}Changed
    }
    " >> ${uiModelPath}/gwtperformance${i}.uidl
    
    registerPath=${domainId}-web/src/main/java/com/example/foo/web/client/app/UIWorkbenchPartRegister.java
    echo "
    package com.example.foo.web.client.app;
    " > $registerPath
    for i in $(seq 1 $numberofEntity); do
    echo "import com.example.foo.web.client.performance.ShowFoo${i}CompositeCommand;" >> $registerPath;
    done
    echo "
    import com.example.foo.web.client.workbench.HeaderPresenter;
    import com.google.inject.Inject;
    import com.google.inject.name.Named;
    import edu.fudan.langlab.uidl.domain.app.client.IWorkbenchMenuContribution;
    import edu.fudan.langlab.uidl.domain.app.client.WorkbenchCategoryImpl;
    import edu.fudan.langlab.uidl.domain.app.client.WorkbenchManager;
    @SuppressWarnings(\"all\")
    public class UIWorkbenchPartRegister {
      @Inject
      private WorkbenchManager workbenchManager;
    
      @Inject
      private HeaderPresenter headerPresenter;
    " >> $registerPath;
    
    for i in $(seq 1 $numberofEntity); do
        echo "@Inject private ShowFoo${i}CompositeCommand showfoo${i};" >> $registerPath
    done
    echo "
      public void initialize() {
        this.workbenchManager.registerHeaderPresenter(this.headerPresenter);
        WorkbenchCategoryImpl c = new WorkbenchCategoryImpl(\"foo\",\"menu\", null);
        workbenchManager.registerPresenterCategory(c);
    " >> $registerPath
    
    for i in $(seq 1 $numberofEntity); do
        echo "workbenchManager.registerCommand(c, showfoo${i});" >> $registerPath;
    done
    
    echo "}
    }" >> $registerPath
    
    done

}
function cleanUp(){
    local domainId=$1
    echo "$domainId"
    find . -name gwtperformance*.edl -exec rm {} \; 
    find . -name gwtperformance*.uidl -exec rm {} \;
    rm -f ${domainId}-web/src/main/java/com/example/foo/web/client/app/UIWorkbenchPartRegister.java
}

function doTest(){
    local domainId=$1
    if [[ -z $domainId ]]; then
        echo "no domainId"
        exit 1
    fi
    shift 1
    local numberofEntity=$1
    if [[ -z numberofEntity ]];then
        echo "no numberof Entity"
        exit 1;
    fi
    shift 1
    local entityOrDatatype=$1
    if [[ -z numberofEntity ]];then
        echo "no entity or datatype"
        exit 1;
    fi

    echo "generateCode $domainId $numberofEntity $entityOrDatatype"
    generateCode $domainId $numberofEntity $entityOrDatatype
    cd ${domainId}-domain  && mvn clean compile -Pxcompile  && mvn clean install  && cd - &&\
        cd ${domainId}-web && mvn clean compile -Pxcompile && cd -
    if [[ $? -ne 0 ]]; then
        echo "error .... "
        exit 1
    fi
    
    cd ${domainId}-web && mvn clean gwt:clean package | tee gwtpackage.log 
    
    echo "======== number of ${entityOrDatatype} ${numberofEntity}" >> gwtperformance.log
    grep "Total time: " gwtpackage.log  >> gwtperformance.log
    echo "" >> gwtperformance.log

    cd ..
    cleanUp $domainId
}

cleanUp foo
doTest foo 30 entity
doTest foo 30 datatype
