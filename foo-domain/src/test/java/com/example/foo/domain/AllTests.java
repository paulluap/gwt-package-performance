package com.example.foo.domain;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import test.com.example.foo.domain.LoginTest;

@RunWith(Suite.class)
@SuiteClasses({ LoginTest.class})
public class AllTests { 

}
