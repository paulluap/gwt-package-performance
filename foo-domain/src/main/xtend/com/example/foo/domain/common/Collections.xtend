package com.example.foo.domain.common;

import java.util.Map

class Collections {
	def static  <K,V> fillEmpty(Map<K,V> results, Iterable<K> allKeys, (K)=>V gen){
		allKeys.forEach[
			if(results.get(it)==null){
				results.put(it,gen.apply(it))
			}
		]
	}
}
