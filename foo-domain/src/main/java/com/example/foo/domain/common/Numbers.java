package com.example.foo.domain.common;

public class Numbers {
	public static Boolean eq(Double m1, Double m2) {
		return new Double(m1 * 1000).intValue() == new Double(m2 * 1000)
				.intValue();
	}
	public static Boolean ge(Double m1, Double m2) {
		return new Double(m1 * 1000).intValue() >= new Double(m2 * 1000)
		.intValue();
	}

	public static Boolean gt(Double m1, Double m2) {
		return new Double(m1 * 1000).intValue() > new Double(m2 * 1000)
				.intValue();
	}

}
