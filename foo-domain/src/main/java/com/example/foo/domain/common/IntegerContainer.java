package com.example.foo.domain.common;

public class IntegerContainer {
    private Integer value;

    public IntegerContainer(Integer value) {
        this.value = value;
    }

    public Integer take(Integer takeValue) {
        Object _xblockexpression = null;
        Integer _xifexpression = null;
        boolean _lessEqualsThan = takeValue.compareTo(this.value) <= 0;
        if(_lessEqualsThan) {
            _xifexpression = takeValue;
        } else {
            _xifexpression = this.value;
        }

        int _minus = this.value.intValue() - _xifexpression.intValue();
        this.value = Integer.valueOf(_minus);
        return _xifexpression;
    }

    public Integer remain() {
        return this.value;
    }
}
