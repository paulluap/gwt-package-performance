package com.example.foo.domain.common;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Dates {
	public static Date create(Integer year, Integer month, Integer date,Integer hourOfDay, Integer minute){
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month-1, date, hourOfDay, minute,0);
		return calendar.getTime();
	}
	
	public static Date create(Integer year, Integer month, Integer date){
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month-1, date,0,0,0);
		return calendar.getTime();
		
	}
	
	public static String formatDate(Date date){
		return new SimpleDateFormat("yyyy-MM-dd").format(date);		
	}
}
