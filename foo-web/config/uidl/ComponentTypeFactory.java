package uidl;

import edu.fudan.langlab.gxt.client.component.ComponentTypeFactory4CriteriaFrom;

public interface ComponentTypeFactory
		extends edu.fudan.langlab.gxt.client.component.ComponentTypeFactory, ComponentTypeFactory4CriteriaFrom {

}
