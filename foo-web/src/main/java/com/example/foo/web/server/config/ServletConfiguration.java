package com.example.foo.web.server.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.example.foo.web.server.servlet.ApplicationRemoteServiceServlet;

@Configuration
public class ServletConfiguration {

	@Bean
	public ServletRegistrationBean servletRegistrationBean() {
		ServletRegistrationBean bean = new ServletRegistrationBean(new ApplicationRemoteServiceServlet(), "/FooWeb/service/*");
		bean.setLoadOnStartup(1);
		return bean;
	}

	//TODO: other servlets

}
