package com.example.foo.web.shared.version;
import org.springframework.stereotype.Service;
import edu.fudan.langlab.uidl.domain.app.client.ClientVersionProvider;
import edu.fudan.langlab.uidl.domain.app.shared.version.IServerVersionProvider;

@Service
public class VersionProvider extends ClientVersionProvider implements IServerVersionProvider{
    
    private final static String VERSION = "gitcommitid";

    @Override
    public String getServerVersion() {
        return VERSION;
    }

    @Override
    public String getClientVersion() {
        return VERSION;
    }
}
