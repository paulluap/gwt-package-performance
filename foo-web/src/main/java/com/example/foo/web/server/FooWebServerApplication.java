package com.example.foo.web.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages= {
	"com.example.foo.web.server",
	"com.example.foo.web.shared",
})
public class FooWebServerApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		System.out.println("======== running ExampleServerApplication as spring boot, nothing special here");
		SpringApplication.run(FooWebServerApplication.class, args);
	}


	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		System.out.println("======== running ExampleServerApplication from container ");
		return builder.sources(FooWebServerApplication.class);
	}

}

