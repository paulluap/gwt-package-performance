package com.example.foo.web.server.service.login.impl;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import com.uniquesoft.gwt.server.service.common.HttpRequestThreadLocal;

import edu.fudan.langlab.domain.security.ICurrentLoginUserService;

@Service
public class CurrentLoginUserServiceImpl implements ICurrentLoginUserService {

	@Override
	public String getCurrentUserId() {
		try {
			HttpServletRequest req = HttpRequestThreadLocal.get();
			Long userid = (Long) req.getSession().getAttribute("__userid__");
			return userid.toString();
		} catch (Exception e) {
			return null;
		}
	}

}

