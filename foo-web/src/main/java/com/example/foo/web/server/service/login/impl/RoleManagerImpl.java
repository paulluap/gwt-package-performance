package com.example.foo.web.server.service.login.impl;

import java.util.Collection;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import edu.fudan.langlab.domain.organization.RoleManager;
import edu.fudan.langlab.domain.security.AppRole;
import edu.fudan.langlab.domain.security.SecurityService;

@Service("com.example.foo.web.server.service.login.impl.RoleManager")
public class RoleManagerImpl implements RoleManager{

    @Autowired  SecurityService securityService;

	@Override
	public Collection<AppRole> getAllRoles() {
        return securityService.getAllRoles();
	}

}
