package com.example.foo.web.server.servlet;

import com.google.gwt.user.server.rpc.RPCRequest;

import edu.fudan.langlab.uidl.domain.app.server.service.common.RequestLoggingServiceServlet;

public class ApplicationRemoteServiceServlet extends RequestLoggingServiceServlet{

	private static final long serialVersionUID = -3515823352359632815L;

	@Override
	protected boolean hasLogin() {
		return getThreadLocalRequest().getSession().getAttribute("__userid__") != null;
	}
	
	@Override
	protected boolean requireSession(RPCRequest rpcRequest) {
		String methodName = rpcRequest.getMethod().getName();
		return  methodName != "login" && 
				methodName != "getLoginUser" && 
				methodName != "isSuperUsr" && 
				methodName != "getServerVersion";
	}

}

