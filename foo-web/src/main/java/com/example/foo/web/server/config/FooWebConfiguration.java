package com.example.foo.web.server.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.context.annotation.Bean;

import edu.fudan.langlab.config.commons.EnableEDLDomain;
import edu.fudan.langlab.config.commons.EnableUIDL;
import edu.fudan.langlab.config.logging.EnableAppLogging;
import edu.fudan.langlab.config.logging.EnableAppLogging.Implementation;
import edu.fudan.langlab.config.nullimpls.EnableAutoRuntimeNullImpls;

@Configuration
@EnableEDLDomain
@EnableUIDL
@EnableAppLogging(Implementation.ENTITY)
@EnableAutoRuntimeNullImpls
public class FooWebConfiguration { 
    @Bean
    public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
        ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
        threadPoolTaskScheduler.setPoolSize(5);
        threadPoolTaskScheduler.setThreadNamePrefix("ThreadPoolTaskScheduler");
        return threadPoolTaskScheduler;
    }
}
