package com.example.foo.web.server.service.login.impl;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.foo.domain.LoginService;
import edu.fudan.langlab.domain.security.AppFunctionService;
import edu.fudan.langlab.domain.security.SecurityService;
import edu.fudan.langlab.domain.security.User;
import edu.fudan.langlab.security.shared.FunctionIdentifier;
import edu.fudan.langlab.security.shared.IFunctionIdentifier;
import edu.fudan.langlab.uidl.domain.app.server.service.login.ISecurityService;

@Service("com.example.foo.web.server.service.login.impl.SecurityServiceImpl")
public class SecurityServiceImpl implements ISecurityService{
	
	@Autowired
	private LoginService loginService;
	@Autowired
	private SecurityService securityService;
	@Autowired
	private AppFunctionService appFunctionService;

	@Override
	public User login(String userid, String passwd) {
		return loginService.login(userid, passwd);
	}

	@Override
	public Collection<? extends IFunctionIdentifier> loadValidFunctions4User(String userid) {
		return this.securityService.getAllRoles4User(userid).stream()
		.map(
			it-> appFunctionService.loadAllFunctions4Role(it.getRoleId())
		)
		.flatMap(Collection::stream)
		.map(it -> new FunctionIdentifier(it.getFunctionId()))
		.collect(Collectors.toList());
		
	}

}

