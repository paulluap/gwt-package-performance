package com.example.foo.web.client.app;

import com.google.inject.Inject;
import edu.fudan.langlab.uidl.domain.app.client.ModuleInitializer;
import edu.fudan.langlab.uidl.domain.app.client.WorkbenchManager;

public class ModuleInitializerImpl implements ModuleInitializer {
  @Inject
  private UIWorkbenchPartRegister workbenchPartRegister;
  
  @Inject
  private WorkbenchManager workbenchManager;

//@Inject
//private DeliverModualInitalizer deliverModualInitalizer;
//@Inject EventBus eventbus
//@Inject Provider<RouterPresenter> industrialparkManagementPresenter
  
  @Override
  public void initializeModule() {
    this.workbenchPartRegister.initialize();
    //taskRegister.initialize();
    //deliverModualInitalizer.registerWorkbenchCommands(workbenchManager);
    //defineStartupScreen();
  }

//@Override
//public void defineStartupScreen(){
//    eventbus.addHandler(ApplicationInitializedEvent::__type__)[app|
//        val router = industrialparkManagementPresenter.get
//        eventbus.fireEvent(new ShowPresenterEvent(router))
//        router.route
//    ]        
//}
}


