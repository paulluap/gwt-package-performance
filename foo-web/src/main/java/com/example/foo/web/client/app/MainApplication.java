package com.example.foo.web.client.app;

import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;

import com.uniquesoft.gwt.client.common.Presenter;

import edu.fudan.langlab.uidl.domain.app.client.BaseApplication;
import edu.fudan.langlab.uidl.domain.app.client.login.LoginPresenter;

public class MainApplication extends BaseApplication{

	@Override
	protected void doOnModuleLoad() {
		MainGinjector.INSTANCE.getApplicationBuilder().initializeModule();
	}

	@Override
	public Presenter createMainPresenter(Procedure1<Presenter> finishNotifier) {
		LoginPresenter loginPresenter = MainGinjector.INSTANCE.getLoginPresenter();
		finishNotifier.apply(loginPresenter);
		return loginPresenter;
	}

}

