package com.example.foo.web.server.config;

import java.util.Arrays;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import edu.fudan.langlab.config.hibernate.IEntityPackagesProvider;

@Configuration
@ComponentScan(
    basePackages={"com.example.foo.domain"}
)
public class FooWebDomainConfiguration { 
	@Bean
	public IEntityPackagesProvider hibDomainPackagesToScan() {
		return ()->{
			return Arrays.asList(
                    "com.example.foo.domain.impl",
                    "com.example.foo.domain.common.impl",
                    "com.example.foo.domain.organization.impl",
                    "com.example.foo.domain.performance.impl"
				);
		};
	}
}
