package com.example.foo.web.client.names;

import com.uniquesoft.gwt.shared.GWTNamedEntity;

public class ObjectNameService extends edu.fudan.langlab.uidl.domain.app.client.names.ObjectNameService {
	@Override
	protected String _internalGetId(final GWTNamedEntity o) {
		return o.getId().toString();
	}

	@Override
	protected String _internalGetName(final GWTNamedEntity o) {
		return o.getName();
	}

}

