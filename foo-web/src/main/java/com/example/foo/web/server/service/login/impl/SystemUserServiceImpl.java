package com.example.foo.web.server.service.login.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uniquesoft.gwt.server.service.common.HttpRequestThreadLocal;

import edu.fudan.langlab.domain.organization.Party;
import edu.fudan.langlab.domain.security.ISystemUserService;
import edu.fudan.langlab.domain.security.User;
import edu.fudan.mylang.pf.IObjectFactory;

@Service("com.example.foo.web.server.service.login.impl.SystemUserServiceImpl")
public class SystemUserServiceImpl implements ISystemUserService{

    @Autowired IObjectFactory objectFactory;

    @Override
    public User getCurrentUser() {
        try{
            HttpServletRequest req = HttpRequestThreadLocal.get(); 
            Long userid = (Long)req.getSession().getAttribute("__userid__");
            if (userid != null){
                return objectFactory.get(User.class, userid);
            }
            return null;
        }catch(Exception e){
            return null;
        }
    }

    @Override
    public Party getCurrentUserAsParty() {
        return null;
    }

    @Override
    public void logout() {
        HttpSession session = HttpRequestThreadLocal.get().getSession();
        session.removeAttribute("__userid__");
    }

    @Override
    public void setCurrentUser(User user) {
        HttpServletRequest req = HttpRequestThreadLocal.get();
        HttpSession session = req.getSession();
        Long id = user.getId();
        session.setAttribute("__userid__", id);
    }

}
