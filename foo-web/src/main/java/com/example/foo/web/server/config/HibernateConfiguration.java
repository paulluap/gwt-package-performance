package com.example.foo.web.server.config;

import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;

import edu.fudan.langlab.config.hibernate.IEntityPackagesProvider;
import edu.fudan.mylang.pf.IObjectFactory;
import edu.fudan.mylang.pf.ObjectFactoryHibernate4Impl;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class HibernateConfiguration {

	//If use property, env.getRequriedProperty()
	@Bean
	public DataSource mySqlDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/foo?useSSL=false");
		dataSource.setUsername("root");
		dataSource.setPassword("");
		return dataSource;
	}

	@Bean
	public LocalSessionFactoryBean sessionFactory(List<IEntityPackagesProvider> packages) {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		Set<String> entityPackages = new HashSet<>();
		for (IEntityPackagesProvider pp: packages) {
			entityPackages.addAll(pp.getPackages());
		}
		log.info("entity packages to scan: " + entityPackages);
		sessionFactory.setPackagesToScan(entityPackages.toArray(new String[0]));

		sessionFactory.setDataSource(mySqlDataSource());
		sessionFactory.setHibernateProperties(hibProperties());
		return sessionFactory;
	}

	@Bean
	public HibernateTransactionManager transactionManager(LocalSessionFactoryBean sessionFactory) {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager();
		transactionManager.setSessionFactory(sessionFactory.getObject());
		return transactionManager;
	}

	@Bean
	public IObjectFactory objectFactory(LocalSessionFactoryBean sessionFactory) {
		ObjectFactoryHibernate4Impl of = new ObjectFactoryHibernate4Impl();
		of.setSessionFactory(sessionFactory.getObject());
		return of;
	}


	private Properties hibProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", "org.hibernate.dialect.MySQL5InnoDBDialect");

		properties.put("hibernate.show_sql", false);
		properties.put("hibernate.format_sql", false);

		properties.put("hibernate.generate_statistics", false);
		properties.put("hibernate.hbm2ddl.auto", "update");
		properties.put("hibernate.max_fetch_depth", 0);

		properties.put("c3p0.acquire_increment", 1);
		properties.put("c3p0.idle_test_period", 100);
		properties.put("c3p0.max_size", 20);
		properties.put("c3p0.max_statements", 0);
		properties.put("c3p0.min_size", 10);
		properties.put("c3p0.timeout", 100);

		properties.put("javax.persistence.validation.mode", "none");

		return properties;
	}
}

