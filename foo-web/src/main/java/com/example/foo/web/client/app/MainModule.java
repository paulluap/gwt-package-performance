package com.example.foo.web.client.app;

import com.example.foo.web.client.names.ObjectNameService;
import com.example.foo.web.shared.version.VersionProvider;
import com.uniquesoft.coreui.client.login.CoreUILoginView;
import com.uniquesoft.coreui.client.nav.CoreUIMenuIconConfig;
import com.uniquesoft.coreui.client.nav.CoreUIMenuIconConfigImpl;
import com.uniquesoft.coreui.client.widgetfactory.CoreUIInternalWidgetProvider;
import com.uniquesoft.coreui.client.widgetfactory.CoreUIWidgetFactoryImpl;
import com.uniquesoft.coreui.client.workbench.CoreUIWorkbenchView;
import com.uniquesoft.gwt.client.common.RuntimeGinjector;
import com.uniquesoft.gwt.client.common.command.ICommandEventsListener;
import com.uniquesoft.gwt.client.common.gin.UIDLBaseGinModule;
import com.uniquesoft.gwt.client.common.name.IObjectNameProvider;

import edu.fudan.langlab.gxt.client.common.InternalWidgetProvider;
import edu.fudan.langlab.gxt.client.pager.Pager2;
import edu.fudan.langlab.gxt.client.widget.IWidgetFactory;
import edu.fudan.langlab.uidl.domain.app.client.ClientVersionProvider;
import edu.fudan.langlab.uidl.domain.app.client.ModuleInitializer;
import edu.fudan.langlab.uidl.domain.app.client.WorkbenchManager;
import edu.fudan.langlab.uidl.domain.app.client.WorkbenchManagerImpl;
import edu.fudan.langlab.uidl.domain.app.client.login.LoginPresenter;
import edu.fudan.langlab.uidl.domain.app.client.login.LoginPresenterView;
import edu.fudan.langlab.uidl.domain.app.client.task.TaskRegister;
import edu.fudan.langlab.uidl.domain.app.client.workbench.ApplicationWorkbenchPresenter;
import edu.fudan.langlab.uidl.domain.app.client.workbench.ApplicationWorkbenchPresenterView;
import edu.fudan.langlab.uidl.domain.log.client.web.CommandEventsListener;

public class MainModule extends UIDLBaseGinModule{

	@Override
	protected void configure() {
		bind(RuntimeGinjector.class).to(MainGinjector.class);
        bind(IObjectNameProvider.class)
                .to(ObjectNameService.class);

        bindPresenter(LoginPresenter.class,
                    LoginPresenterView.class,
                    CoreUILoginView.class);
                        
                        
        bindPresenter(ApplicationWorkbenchPresenter.class,
                    ApplicationWorkbenchPresenterView.class,
                    CoreUIWorkbenchView.class);

        bind(CoreUIMenuIconConfig.class).to(CoreUIMenuIconConfigImpl.class)
                        .asEagerSingleton();

        bind(IWidgetFactory.class).to(CoreUIWidgetFactoryImpl.class);
        bind(InternalWidgetProvider.class).to(CoreUIInternalWidgetProvider.class);
                
        bind(WorkbenchManager.class).to(WorkbenchManagerImpl.class)
                .asEagerSingleton();
            
        bind(ICommandEventsListener.class).to(CommandEventsListener.class).asEagerSingleton();
        bind(ModuleInitializer.class).to(ModuleInitializerImpl.class);
        bind(UIWorkbenchPartRegister.class);
        bind(Pager2.class);
        bind(TaskRegister.class).asEagerSingleton();
        bind(ClientVersionProvider.class).to(VersionProvider.class);
		
	}

}

