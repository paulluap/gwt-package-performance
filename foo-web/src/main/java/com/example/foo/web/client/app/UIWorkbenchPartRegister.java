
    package com.example.foo.web.client.app;
    
import com.example.foo.web.client.performance.ShowFoo1CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo2CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo3CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo4CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo5CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo6CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo7CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo8CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo9CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo10CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo11CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo12CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo13CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo14CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo15CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo16CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo17CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo18CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo19CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo20CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo21CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo22CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo23CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo24CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo25CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo26CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo27CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo28CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo29CompositeCommand;
import com.example.foo.web.client.performance.ShowFoo30CompositeCommand;

    import com.example.foo.web.client.workbench.HeaderPresenter;
    import com.google.inject.Inject;
    import com.google.inject.name.Named;
    import edu.fudan.langlab.uidl.domain.app.client.IWorkbenchMenuContribution;
    import edu.fudan.langlab.uidl.domain.app.client.WorkbenchCategoryImpl;
    import edu.fudan.langlab.uidl.domain.app.client.WorkbenchManager;
    @SuppressWarnings("all")
    public class UIWorkbenchPartRegister {
      @Inject
      private WorkbenchManager workbenchManager;
    
      @Inject
      private HeaderPresenter headerPresenter;
    
@Inject private ShowFoo1CompositeCommand showfoo1;
@Inject private ShowFoo2CompositeCommand showfoo2;
@Inject private ShowFoo3CompositeCommand showfoo3;
@Inject private ShowFoo4CompositeCommand showfoo4;
@Inject private ShowFoo5CompositeCommand showfoo5;
@Inject private ShowFoo6CompositeCommand showfoo6;
@Inject private ShowFoo7CompositeCommand showfoo7;
@Inject private ShowFoo8CompositeCommand showfoo8;
@Inject private ShowFoo9CompositeCommand showfoo9;
@Inject private ShowFoo10CompositeCommand showfoo10;
@Inject private ShowFoo11CompositeCommand showfoo11;
@Inject private ShowFoo12CompositeCommand showfoo12;
@Inject private ShowFoo13CompositeCommand showfoo13;
@Inject private ShowFoo14CompositeCommand showfoo14;
@Inject private ShowFoo15CompositeCommand showfoo15;
@Inject private ShowFoo16CompositeCommand showfoo16;
@Inject private ShowFoo17CompositeCommand showfoo17;
@Inject private ShowFoo18CompositeCommand showfoo18;
@Inject private ShowFoo19CompositeCommand showfoo19;
@Inject private ShowFoo20CompositeCommand showfoo20;
@Inject private ShowFoo21CompositeCommand showfoo21;
@Inject private ShowFoo22CompositeCommand showfoo22;
@Inject private ShowFoo23CompositeCommand showfoo23;
@Inject private ShowFoo24CompositeCommand showfoo24;
@Inject private ShowFoo25CompositeCommand showfoo25;
@Inject private ShowFoo26CompositeCommand showfoo26;
@Inject private ShowFoo27CompositeCommand showfoo27;
@Inject private ShowFoo28CompositeCommand showfoo28;
@Inject private ShowFoo29CompositeCommand showfoo29;
@Inject private ShowFoo30CompositeCommand showfoo30;

      public void initialize() {
        this.workbenchManager.registerHeaderPresenter(this.headerPresenter);
        WorkbenchCategoryImpl c = new WorkbenchCategoryImpl("foo","menu", null);
        workbenchManager.registerPresenterCategory(c);
    
workbenchManager.registerCommand(c, showfoo1);
workbenchManager.registerCommand(c, showfoo2);
workbenchManager.registerCommand(c, showfoo3);
workbenchManager.registerCommand(c, showfoo4);
workbenchManager.registerCommand(c, showfoo5);
workbenchManager.registerCommand(c, showfoo6);
workbenchManager.registerCommand(c, showfoo7);
workbenchManager.registerCommand(c, showfoo8);
workbenchManager.registerCommand(c, showfoo9);
workbenchManager.registerCommand(c, showfoo10);
workbenchManager.registerCommand(c, showfoo11);
workbenchManager.registerCommand(c, showfoo12);
workbenchManager.registerCommand(c, showfoo13);
workbenchManager.registerCommand(c, showfoo14);
workbenchManager.registerCommand(c, showfoo15);
workbenchManager.registerCommand(c, showfoo16);
workbenchManager.registerCommand(c, showfoo17);
workbenchManager.registerCommand(c, showfoo18);
workbenchManager.registerCommand(c, showfoo19);
workbenchManager.registerCommand(c, showfoo20);
workbenchManager.registerCommand(c, showfoo21);
workbenchManager.registerCommand(c, showfoo22);
workbenchManager.registerCommand(c, showfoo23);
workbenchManager.registerCommand(c, showfoo24);
workbenchManager.registerCommand(c, showfoo25);
workbenchManager.registerCommand(c, showfoo26);
workbenchManager.registerCommand(c, showfoo27);
workbenchManager.registerCommand(c, showfoo28);
workbenchManager.registerCommand(c, showfoo29);
workbenchManager.registerCommand(c, showfoo30);
}
    }
